from tiles import Player, Tile


def load_level(filename):
    filename = "data/" + filename
    with open(filename, 'r') as mapFile:
        level_map = [line.strip() for line in mapFile]
    max_width = max(map(len, level_map))
    return list(map(lambda x: list(x.ljust(max_width, '.')), level_map))


def generate_level(level,hero,tiles):
    new_player, x, y = None, None, None
    for y in range(len(level)):
        for x in range(len(level[y])):
            if level[y][x] == '.':
                Tile('empty', x, y,tiles)
            elif level[y][x] == '#':
                Tile('wall', x, y,tiles)
            elif level[y][x] == '@':
                Tile('empty', x, y,tiles)
                new_player = Player(x, y,hero)
                level[y][x] = "."
    return new_player, x, y
